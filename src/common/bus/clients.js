import Vue from 'vue';
export const ClientBus = new Vue({
  methods: {
    /**
     * 弹出新增窗口
     */
    showAddClientDialog() {
      this.$emit('showAddClientDialog');
    },
    /**
     * 弹出编辑窗口
     * @param {string} clientId
     */
    showEditClientDialog(clientId = null) {
      this.$emit('showEditClientDialog', clientId);
    },
    /**
     * 刷新表格
     */
    refreshTable() {
      this.$emit('refreshTable');
    }
  }
});
