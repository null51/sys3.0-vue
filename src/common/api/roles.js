import axios from '@common/plugins/Axios';

/**
 * 角色管理
 */
class Role {
  /**
   * 角色查询-分页
   * @param{Number} pageSize
   * @param{Number} pageNumber
   * @param{Object} query
   * @return {axios.requestPromise | *}
   */
  getRoleByPage(pageSize, pageNumber, query) {
    return axios.request({
      url: `/business/role/page`,
      method: 'get',
      params: {...query, pageSize, pageNumber}
    });
  }

  /**
   * 获取相关系统
   * @return{Object} Promise
   */
  getRelatedClients() {
    return axios.request({
      url: `/business/role/relatedClients`,
      method: 'get',
    });
  }

  /**
   * 获取角色类型
   * @return{Object} Promise
   */
  getRoleTypes() {
    return axios.request({
      url: `/business/role/types`,
      method: 'get',
    });
  }

  /**
   * 获取角色等级
   * @return{Object} Promise
   */
  getRoleLevels() {
    return axios.request({
      url: `/business/role/levels`,
      method: 'get',
    });
  }

  /**
   * 获取应用系统
   * @param{String} roleId
   * @return{Object} Promise
   */
  getAvailableClients(roleId) {
    if (roleId === '') {
      return axios.request({
        url: `/business/role/availableClients`,
        method: 'get'
      });
    } else {
      return axios.request({
        url: `/business/role/availableClients`,
        method: 'get',
        params: {
          roleId: roleId
        }
      });
    }
  }

  /**
   * 新增角色
   * @param{Object} role
   * @return {axios.requestPromise | *}
   */
  addRole(role) {
    return axios.request({
      url: `/business/role/add`,
      method: 'post',
      data: role
    });
  }

  /**
   * 编辑角色
   * @param{Object} role
   * @return {axios.requestPromise | *}
   */
  editRole(role) {
    return axios.request({
      url: `/business/role/edit/${role.roleId}`,
      method: 'put',
      data: role
    });
  }

  /**
   * 删除角色
   * @param{String} roleId
   * @return {axios.requestPromise | *}
   */
  deleteRole(roleId) {
    return axios.request({
      url: `/business/role/delete/${roleId}`,
      method: 'delete',
    });
  }

  /**
   * 打开编辑页面是查询角色信息
   * @param{String} roleId
   * @return {Object} Promise
   */
  getRoleByRoleId(roleId) {
    return axios.request({
      url: `/business/role/get/${roleId}`,
      method: 'get'
    });
  }

  /**
   * 绑定用户
   * @param{String} roleId
   * @param{String} userId
   * @return {Object} Promise
   */
  addRoleUser(roleId, userId) {
    return axios.request({
      url: `/business/role/bindUser/role/${roleId}/user/${userId}`,
      method: 'put'
    });
  }

  /**
   * 移除用户角色
   * @param{String} roleId
   * @param{String} userId
   * @return {Object} Promise
   */
  removeRoleUser(roleId, userId) {
    return axios.request({
      url: `/business/role/unbindRole/role/${roleId}/user/${userId}`,
      method: 'delete'
    });
  }

  /**
   * 查询角色
   * @param{Number} pageSize
   * @param{Number} pageNumber
   * @param{Object} query
   * @return {Object} Promise
   */
  getRolesByPage(pageSize, pageNumber, query) {
    return axios.request({
      url: `/business/user/roles/page`,
      method: 'get',
      params: {...query, pageSize, pageNumber}
    });
  }

  /**
   * 查询用户
   * @param{Number} pageSize
   * @param{Number} pageNumber
   * @param{Object} query
   * @return {Object} Promise
   */
  getUsersByPage(pageSize, pageNumber, query) {
    return axios.request({
      url: `/business/role/users/page`,
      method: 'get',
      params: {...query, pageSize, pageNumber}
    });
  }

  /**
   * 获取资源树
   * @param {String} clientId
   * @param {String} roleId
   * @return {*}
   */
  getSyncResourceForTree({clientId, roleId}) {
    return axios.request({
      url: `/business/role/tree`,
      method: 'get',
      params: {clientId, roleId}
    });
  }
}

const roles = new Role();
export default roles;
