import auth from './auth';
import clients from './clients';
import resources from './resources';
import users from './users';
import roles from './roles';


export default {
  auth,
  clients,
  resources,
  users,
  roles
};
