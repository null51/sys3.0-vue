import axios from '@common/plugins/Axios';

/**
 * 动态SQL查询接口集合
 */
class ComTag {
  /**
   * 构造器
   */
  constructor() {

  }

  /**
   * 获取动态SQL
   * @param {String} code
   * @param {Object} query
   * @return {*}
   */
  getComTag(code, query) {
    return axios.request({
      url: `/business/dictionary/${code}`,
      method: 'get',
      params: query
    });
  }
}
const comTag = new ComTag();
export default comTag;
