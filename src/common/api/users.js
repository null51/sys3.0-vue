import axios from '@common/plugins/Axios';
import comTag from './comTag';

/**
 * 用户管理页面接口
 */
class User {
  /**
   * tagcode
   */
  constructor() {
    this.tagCode = {
      userStateCode: 'QRY_COMMON_STATE',
      userAccountTypeCode: 'QRY_USER_ACCOUNT_TYPE'
    };
  }
  /**
   * 用户管理-分页
   * @param{Number} pageSize
   * @param{Number} pageNumber
   * @param{Object} query
   * @return {axios.requestPromise | *}
   */
  getUserByPage(pageSize, pageNumber, query) {
    return axios.request({
      url: `/business/user/page`,
      method: 'get',
      params: {...query, pageSize, pageNumber}
    });
  }

  /**
   * 查询用户状态
   * @return {*}
   */
  getUserStates() {
    return comTag.getComTag(this.tagCode.userStateCode);
  }

  /**
   * 查询初始密码
   * @return {*}
   */
  getDefaultPassword() {
    return axios.request({
      url: `/business/user/getDefaultPassword`,
      method: 'get'
    });
  }
  /**
   * 查询账号类型
   * @return {*}
   */
  getAccountTypes() {
    return comTag.getComTag(this.tagCode.userAccountTypeCode);
  }

  /**
   * 打开编辑页面是查询用户信息
   * @param{String} userId
   * @return {Object} Promise
   */
  getUserByUserId(userId) {
    return axios.request({
      url: `/business/user/get/${userId}`,
      method: 'get'
    });
  }

  /**
   * 新增用户
   * @param{Object} user
   * @return {axios.requestPromise | *}
   */
  addUser(user) {
    return axios.request({
      url: `/business/user/add`,
      method: 'post',
      data: user
    });
  }

  /**
   * 编辑用户
   * @param{Object} user
   * @return {axios.requestPromise | *}
   */
  editUser(user) {
    return axios.request({
      url: `/business/user/edit/${user.userId}`,
      method: 'put',
      data: user
    });
  }

  /**
   * 重置密码
   * @param{String} userId
   * @return {axios.requestPromise | *}
   */
  resetPassword(userId) {
    return axios.request({
      url: `/business/user/reset/${userId}`,
      method: 'put'
    });
  }

  /**
   * 注销
   * @param{String} userId
   * @return {axios.requestPromise | *}
   */
  cancel(userId) {
    return axios.request({
      url: `/business/user/cancel/${userId}`,
      method: 'put'
    });
  }

  /**
   * 冻结
   * @param{StrinG} userId
   * @return {axios.requestPromise | *}
   */
  freeze(userId) {
    return axios.request({
      url: `/business/user/freeze/${userId}`,
      method: 'put'
    });
  }

  /**
   * 解冻
   * @param{String} userId
   * @return {axios.requestPromise | *}
   */
  unfreeze(userId) {
    return axios.request({
      url: `/business/user/unfreeze/${userId}`,
      method: 'put'
    });
  }

  /**
   * 绑定角色
   * @param{String} userId
   * @param{String} roleId
   * @return {Object} Promise
   */
  addUserRole(userId, roleId) {
    return axios.request({
      url: `/business/user/bindRole/user/${userId}/role/${roleId}`,
      method: 'put'
    });
  }

  /**
   * 移除用户角色
   * @param{String} userId
   * @param{String} roleId
   * @return {Object} Promise
   */
  removeUserRole(userId, roleId) {
    return axios.request({
      url: `/business/user/unbindRole/user/${userId}/role/${roleId}`,
      method: 'delete'
    });
  }

  /**
   * 查询相关系统
   * @param{String} userId
   * @return {Object} Promise
   */
  getRelatedClients(userId) {
    return axios.request({
      url: `/business/user/getRelatedClients/${userId}`,
      method: 'get'
    });
  }
}
const users = new User();
export default users;
