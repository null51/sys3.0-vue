import axios from '@common/plugins/Axios';
import config from '@/config';

/**
 * 登录鉴权模块接口集合
 */
class Auth {
  /**
   * 构造器
   */
  constructor() {
  }

  /**
   * 登录接口
   * @param {String} loginAccount
   * @param {String} password
   * @return {AxiosPromise | *}
   */
  login({loginAccount, password}) {
    return axios.request({
      url: `/login`,
      method: 'post',
      params: {
        loginAccount, password,
        clientCode: config.clientCode
      }
    });
  }

  /**
   * 获取角色列表
   * @param {String} loginAccount
   * @return {AxiosPromise | *}
   */
  fetchRoles({loginAccount}) {
    return axios.request({
      url: `/auth/roles`,
      method: 'get',
      params: {
        loginAccount,
        clientCode: config.clientCode
      }
    });
  }

  /**
   * 获取权限列表
   * @param {String} loginAccount
   * @return {AxiosPromise | *}
   */
  fetchPrivileges({loginAccount}) {
    return axios.request({
      url: `/auth/privileges`,
      method: 'get',
      params: {
        loginAccount,
        clientCode: config.clientCode
      }
    });
  }
}
const auth = new Auth();
export default auth;
