import axios from '@common/plugins/Axios';
import comTag from './comTag';

/**
 * 系统注册模块接口集合
 */
class Client {
  /**
   * 基础构造器
   */
  constructor() {
    this.tagCode = {
      accessStrategy: 'QRY_CLIENT_ACCESS_STRATEGY',
      clientType: 'QRY_CLIENT_TYPE'
    };
  }

  /**
   * 获取系统列表-分页
   * @param {number} pageSize
   * @param {number} pageNumber
   * @param {Object} query
   * @return {axios.requestPromise | *}
   */
  getClientsByPage(pageSize, pageNumber, query) {
    return axios.request({
      url: `/api/clients/page`,
      method: 'get',
      params: {
        ...query,
        pageSize,
        pageNumber
      }
    });
  }

  /**
   * 获取系统树结构
   * @param {Object} query
   * @return {axios.requestPromise | *}
   */
  getClientsForTree(query) {
    return axios.request({
      url: `/api/clients/tree`,
      method: 'get',
      params: query
    });
  }

  /**
   * 获取系统列表
   * @param {Object} query
   * @return {axios.requestPromise | *}
   */
  getClients(query) {
    return axios.request({
      url: `/api/clients`,
      method: 'get',
      params: query
    });
  }

  /**
   * 根据主键获取系统对象
   * @param {string} clientId
   * @return {axios.requestPromise | *}
   */
  getClientsByClientId(clientId) {
    return axios.request({
      url: `/api/clients/${clientId}`,
      method: 'get'
    });
  }

  /**
   * 获取访问控制
   * @return {axios.requestPromise | *}
   */
  getAccessStrategies() {
    return comTag.getComTag(this.tagCode.accessStrategy);
  }

  /**
   * 获取系统类型
   * @return {axios.requestPromise | *}
   */
  getClientType() {
    return comTag.getComTag(this.tagCode.clientType);
  }

  /**
   * 获取最新的展示顺序
   * @return {axios.requestPromise | *}
   */
  getLastestDisplayOrder() {
    return axios.request({
      url: `/api/clients/order`,
      method: 'get'
    });
  }

  /**
   * 禁用系统
   * @param {string} clientId
   * @return {axios.requestPromise | *}
   */
  disableClient(clientId) {
    return axios.request({
      url: `/api/clients/${clientId}/disable`,
      method: 'put'
    });
  }

  /**
   * 激活系统
   * @param {string} clientId
   * @return {axios.requestPromise | *}
   */
  enableClient(clientId) {
    return axios.request({
      url: `/api/clients/${clientId}/enable`,
      method: 'put'
    });
  }

  /**
   * 新增系统
   * @param {Object}client
   * @return {axios.requestPromise | *}
   */
  addClient(client) {
    return axios.request({
      url: `/api/clients`,
      method: 'post',
      data: client
    });
  }

  /**
   * 编辑系统
   * @param {Object} client
   * @return {axios.requestPromise | *}
   */
  editClient(client) {
    return axios.request({
      url: `/api/clients/${client.clientId}`,
      method: 'put',
      data: client
    });
  }
}
const clients = new Client();
export default clients;
