import axios from '@common/plugins/Axios';
import comTag from './comTag';

/**
 * 资源管理API
 */
class Resources {
  /**
   * 构造器
   */
  constructor() {
    this.tagCode = {
      resourceType: 'QRY_RESOURCE_TYPE',
      elementType: 'QRY_RESOURCE_ELEMENT_TYPE'
    };
  }

  /**
   * 懒加载-获取资源节点
   * @param {String} clientId
   * @param {String} pResourceId
   * @return {*}
   */
  getResourcesForTree({clientId, pResourceId = null}) {
    return axios.request({
      url: `/api/resources/tree`,
      method: 'get',
      params: {clientId, pResourceId}
    });
  }

  /**
   * 获取资源
   * @param {String} resourceId
   * @return {*}
   */
  getResourceByResourceId(resourceId) {
    return axios.request({
      url: `/api/resources/${resourceId}`,
      method: 'get'
    });
  }

  /**
   * 获取最新的展示顺序
   * @param {String} clientId
   * @param {String} pResourceId
   * @return {*}
   */
  getLastestDisplayOrder({clientId, pResourceId = null}) {
    return axios.request({
      url: `/api/resources/order`,
      method: 'get',
      params: {clientId, pResourceId}
    });
  }

  /**
   * 获取资源类型
   * @return {*}
   */
  getResourceType() {
    return comTag.getComTag(this.tagCode.resourceType);
  }

  /**
   * 获取元素类型
   * @return {*}
   */
  getElementType() {
    return comTag.getComTag(this.tagCode.elementType);
  }

  /**
   * 新增资源
   * @param {Object}resource
   * @return {*}
   */
  addResource(resource) {
    return axios.request({
      url: `/api/resources`,
      method: 'post',
      data: resource
    });
  }

  /**
   * 编辑资源
   * @param {Object} resource
   * @return {*}
   */
  editResource(resource) {
    return axios.request({
      url: `/api/resources/${resource.resourceId}`,
      method: 'put',
      data: resource
    });
  }

  /**
   * 删除资源
   * @param {String} resourceId
   * @return {*}
   */
  removeResource(resourceId) {
    return axios.request({
      url: `/api/resources/${resourceId}`,
      method: 'delete'
    });
  }
}

const resources = new Resources();
export default resources;
