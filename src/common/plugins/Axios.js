import Axios from 'axios';
import store from '@store/index';
import router from '@router/index';
import {Notification} from 'element-ui';

/**
 * axios实例
 * @type {AxiosInstance}
 */
let instance = Axios.create({
  headers: {
    'x_requested_with': 'XMLHttpRequest'
  }
});

/**
 * axios请求过滤器
 */
instance.interceptors.request.use(
  (config) => {
    if (store.state.auth.token) { // 判断是否存在token，如果存在的话，则每个http header都加上token
      config.headers.Authorization = store.state.auth.token;
    }
    return config;
  },
  (err) => {
    return Promise.reject(err);
  });

/**
 * axios响应过滤器
 */
instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    console.error(error);
    if (error.response.status === 401 || error.response.status === 403) {
      store.dispatch('auth/logout').then(() => {
        router.replace('/login');
        Notification.error({
          title: '提示',
          message: error.response.data.meta.message
        });
      }).catch((err) => {
        console.error(err);
      });
    } else {
      return Promise.reject(error.response);
    }
  });

export default instance;


