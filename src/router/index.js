import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@store/index';
import constants from '@common/constants/constants';

Vue.use(VueRouter);

/**
 * 创建一个新路由
 * @return {VueRouter}
 */
const createRouter = () => new VueRouter({
  mode: 'hash',
  routes: store.state.route.constantRoutes
});
const router = createRouter();

/**
 * 重置路由
 * 此方法存在的原因为：在登出时主动清除了登录信息之后（包括localstorage）
 * 页面并未刷新，vue-router中动态添加的路由依旧存在，在不刷新页面的情况下，
 * 再次登录，会再次动态添加路由至原有路由，会报出路由已经被添加的情况，
 * 且原有路由会被缓存。
 * 此方法会重新替换原有路由为新的路由，新路由中只有静态路由（如登录页），
 * 避免报错
 */
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // the relevant part
}

/**
 * 全局路由钩子
 */
router.beforeEach((to, from, next) => {
  if (to.path === '/' && localStorage.getItem(constants.TOKEN_KEY) === null) {
    next('/login');
  }
  // 如果路由需要权限访问，并且存在token，则拿当前token进入路由，
  // 否则视为没有权限，跳转到登录页
  // 如果路由不需要权限访问，直接跳转到目标页面
  if (to.meta.requireAuth === true) {
    if (localStorage.getItem(constants.TOKEN_KEY) !== null) {
      next();
    } else {
      next('/login');
    }
  } else {
    next();
  }
});

/**
 * 路由导航异常handler
 */
router.onError(function(error) {
  console.error(error);
  console.log('导航出错了');
});

export default router;

