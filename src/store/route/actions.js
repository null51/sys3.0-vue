import MainSection from '@components/layouts/MainSection';
import router from '@router/index';
import {resetRouter} from '@router/index';

export default {
  /**
   * 根据已经登录之后获取的权限信息生成动态路由
   * @param {Object} context
   * @param {Object} payload
   * @return {Promise<any>}
   */
  setRoutes(context, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        // 清除动态路由和已加载标识
        context.commit('clear');
        // 获取权限信息
        const menus = context.rootGetters['auth/menus'];
        // 设置基础父路由
        let routes = [{
          name: 'main',
          path: '/',
          meta: {
            requireAuth: true
          },
          component: MainSection,
          redirect: {name: menus[0].privilegeCode}, // 如果访问'/'路径，访问能访问的第一个页面
          children: []
        }];
        // 向父路由中添加子路由
        routes[0].children = menus.filter((menu) => {
          return context.state.routesMapping.hasOwnProperty(menu.privilegeCode);
        }).map((menu) => {
          return {
            name: menu.privilegeCode,
            path: menu.resourceUrl,
            meta: {
              requireAuth: true
            },
            component: context.state.routesMapping[menu.privilegeCode],
          };
        });
        // 设置动态路由数据
        context.commit('setDynamicRoutes', routes);
        // 设置已加载标识
        context.commit('setLoaded', true);
        // 重置路由
        resetRouter();
        // 添加动态路由
        router.addRoutes(routes);
        // 跳转至可以访问的第一个页面
        router.replace({name: menus[0].privilegeCode});
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  }
};
