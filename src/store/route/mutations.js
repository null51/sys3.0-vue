export default {
  setLoaded(state, loaded) {
    state.loaded = loaded;
  },
  setDynamicRoutes(state, dynamicRoutes) {
    state.dynamicRoutes = dynamicRoutes;
  },
  clear(state) {
    state.loaded = false;
    state.dynamicRoutes = [];
  }
};

