import IndexSection from '@components/IndexSection';
import ClientSection from '@components/clients/ClientSection';
import ResourceSection from '@components/resources/ResourceSection';
import UserSection from '@components/users/UserSection';
import RoleSection from '@components/roles/RoleSection';
import store from '@store/index';
// const IndexSection = (r) => require(['@components/IndexSection'], r);
// const ClientSection = (r) => require(['@components/clients/ClientSection'], r);
// const ResourceSection = (r) => require(['@components/resources/ResourceSection'], r);
// const UserSection = (r) => require(['@components/users/UserSection'], r);
// const RoleSection = (r) => require(['@components/roles/RoleSection'], r);

// const login = r => require.ensure([], () => r(require('@/page/login')), 'login');
export default {
  // 静态路由
  constantRoutes: [{
    name: 'login',
    path: '/login',
    meta: {
      requireAuth: false
    },
    component: (r) => require(['@components/LoginSection'], r),
  }, {
    path: '/logout',
    name: 'logout',
    meta: {
      requireAuth: false
    },
    beforeEnter(to, from, next) {
      if (store.state.auth.token) {
        store.dispatch('auth/logout').then(() => {
          next({name: 'login', replace: true});
        }).catch((err) => {
          console.error(err);
        });
      } else {
        next({name: 'login', replace: true});
      }
    }
  }],
  // 路由载入标识
  loaded: false,
  // 动态路由
  dynamicRoutes: [],
  // 路由映射 - {权限编码: 对应组件}
  routesMapping: {
    'SYS-INDEX': IndexSection,
    'SYS-MANAGE-CLIENT:INDEX': ClientSection,
    'SYS-MANAGE-RESOURCE:INDEX': ResourceSection,
    'SYS-MANAGE-USER:INDEX': UserSection,
    'SYS-MANAGE-ROLE:INDEX': RoleSection,
  },
  defaultRouteName: null
};
