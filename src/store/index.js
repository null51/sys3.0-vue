import Vue from 'vue';
import Vuex from 'vuex';

import auth from '@store/auth/index';
import client from '@store/client/index';
import resources from '@store/resource/index';
import route from '@store/route/index';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    client: client,
    resources: resources,
    auth: auth,
    route: route
  }
});
