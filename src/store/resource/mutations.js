export default {
  setResource(state, resource) {
    state.resource = resource;
  },
  setAction(state, action) {
    state.action = action;
  },
  setShow(state, show) {
    state.show = show;
  }
};

