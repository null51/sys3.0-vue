import api from '@common/api/index';

export default {
  setResource(context, resourceId) {
    return new Promise(async (resolve, reject) => {
      try {
        const resource = await api.resources.getResourceByResourceId(resourceId);
        context.commit('setResource', resource.data.data);
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  }
};

