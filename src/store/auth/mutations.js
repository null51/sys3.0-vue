import Constants from '@common/constants/constants';

export default {
  setUser(state, payload) {
    localStorage.setItem(Constants.USER_KEY, JSON.stringify(payload.user));
    state.user = payload.user;
  },
  setToken(state, payload) {
    localStorage.setItem(Constants.TOKEN_KEY, payload.token);
    state.token = payload.token;
  },
  setRoles(state, payload) {
    state.roles = payload.roles;
  },
  setResources(state, payload) {
    state.resources = payload.resources;
  },
  clear(state) {
    localStorage.removeItem(Constants.USER_KEY);
    localStorage.removeItem(Constants.TOKEN_KEY);
    state.user = null;
    state.roles = null;
    state.token = null;
    state.resources = null;
  }
};
