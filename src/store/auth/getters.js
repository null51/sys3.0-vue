// import {makeTree} from '@common/util';
//
// export default {
//   menus: (state) => {
//     return state.resources.filter((resource) => {
//       return resource.resourceType === 'MENU';
//     });
//   },
//   elements: (state) => {
//     return state.resources.filter((resource) => {
//       return resource.resourceType === 'ELEMENT';
//     });
//   },
//   resourcesTree: (state) => {
//     return makeTree('resourceId', 'pResourceId', state.resources);
//   },
//   menusTree: (state) => {
//     const menus = state.resources.filter((resource) => {
//       return resource.resourceType === 'MENU';
//     });
//     return makeTree('resourceId', 'pResourceId', menus);
//   },
//   elementsTree: (state) => {
//     const elements = state.resources.filter((resource) => {
//       return resource.resourceType === 'ELEMENT';
//     });
//     return makeTree('resourceId', 'pResourceId', elements);
//   }
// };
//
import {makeTree} from '@common/util';
import constants from '@common/constants/constants';

export default {
  menus: (state) => {
    return state.resources === null ? null : state.resources.filter((resource) => {
      return resource.resourceType === 'MENU';
    });
  },
  elements: (state) => {
    return state.resources === null ? null : state.resources.filter((resource) => {
      return resource.resourceType === 'ELEMENT';
    });
  },
  resourcesTree: (state) => {
    return state.resources === null ? null : makeTree('resourceId', 'pResourceId', state.resources);
  },
  menusTree: (state) => {
    return state.resources === null ? null : (function() {
      const menus = state.resources.filter((resource) => {
        return resource.resourceType === 'MENU';
      });
      return makeTree('resourceId', 'pResourceId', menus);
    })();
  },
  elementsTree: (state) => {
    return state.resources === null ? null : (function() {
      const menus = state.resources.filter((resource) => {
        return resource.resourceType === 'ELEMENT';
      });
      return makeTree('resourceId', 'pResourceId', menus);
    })();
  },
  /**
   * 获取可进入的第一个菜单的地址，用于登录后重定向
   * @param {Object} state
   * @return {*}
   */
  firstMenuUrl: (state) => {
    if (state.resources !== null) {
      let firstMenu = state.resources.filter((resource) => {
        return resource.resourceUrl !== '' && resource.resourceUrl !== null;
      });
      let indexMenu = state.resources.filter((resource) => {
        return resource.previlegeCode === constants.INDEX_CODE;
      });

      if (firstMenu.length !== 0) {
        return firstMenu[0].resourceUrl;
      } else if (indexMenu.length !== 0) {
        return indexMenu[0].resourceUrl;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
};

