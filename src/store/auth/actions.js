import api from '@common/api/index';
import router from '@router/index';
import {resetRouter} from '@router/index';
export default {
  login({commit}, {loginAccount, password}) {
    return new Promise((resolve, reject) => {
      // 发出登录请求
      api.auth.login({loginAccount, password}).then((response) => {
        // 提交用户信息至vuex
        commit('setUser', {user: response.data.data.user});
        // 提交accessToken至vuex
        commit('setToken', {token: response.data.data.accessToken});
        resolve({loginAccount: response.data.data.user.loginAcct});
      }).catch((err) => {
        reject(err);
      });
    });
  },
  getRolesAndResources({commit, state}) {
    return new Promise((resolve, reject) => {
      const loginAccount = state.user.loginAcct;
      // 获取角色信息与权限信息
      Promise.all([api.auth.fetchRoles({loginAccount}), api.auth.fetchPrivileges({loginAccount})])
      .then(([roleResponse, resourceResponse]) => {
        // 提交角色信息至vuex
        commit('setRoles', {
          roles: roleResponse.data.data
        });
        // 提交权限信息至vuex
        commit('setResources', {
          resources: resourceResponse.data.data
        });
        resolve();
      }).catch((err) => {
        reject(err);
      });
    });
  },
  logout({commit, state}) {
    // 清除用户信息、accessToken、角色信息、权限信息，包括localStorage
    commit('clear');
    // 清除动态路由信息
    commit('route/clear', null, {root: true});
    // 重置路由，详见注释
    resetRouter();
    // 跳转至登录页面
    router.replace('/login');
  }
};
