import Vue from 'vue';
import App from './App.vue';
import router from '@router/index';
import store from '@store/index';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import config from '@/config';
import axios from '@common/plugins/Axios';
import api from '@common/api/index';
import constants from '@common/constants/constants';

Vue.use(ElementUI);
Vue.prototype.$http = axios;
Vue.prototype.$api = api;
Vue.prototype.$previlegeCodes = config.previlegeCodes;

Vue.config.productionTip = false;

/**
 * TODO：
 * IE11下，需要promise polyfill
 * bug: 资源管理删除失败后右侧不隐藏
 * 个人信息展示
 * 右上角登录展示
 * bug: 资源管理-新增-显示顺序 按加减按钮应该每次加减一
 * 代码审查
 * 首页添加请求
 * bug: 弹框内栅格布局
 */


Vue.directive('shiro', {
  inserted(el, binding) {
    if (binding.arg === 'hasPermission') {
      const code = binding.value;
      let codes = store.state.auth.resources.map((resource) => {
        return resource.resourceType === 'ELEMENT' ? resource.privilegeCode : null;
      }).filter((value) => {
        return value !== null;
      });
      /**
       * FIXME
       * 在用户管理中，冻结用户后，再解冻，授权按钮并未正常显示
       */
      if (codes.indexOf(code) === -1) {
        el.parentNode.removeChild(el);
      }
    }
  }
});

new Vue({
  router,
  store,
  render: (h) => h(App),
  mounted() {
    // 页面刷新时，重新赋值token、user、resources
    const user = window.localStorage.getItem(constants.USER_KEY);
    const token = window.localStorage.getItem(constants.TOKEN_KEY);
    if (token) {
      store.commit('auth/setToken', {
        token: token
      });
    }
    if (user) {
      store.commit('auth/setUser', {
        user: JSON.parse(user)
      });
    }
    store.dispatch('auth/getRolesAndResources').then(() => {
      store.dispatch('route/setRoutes').catch((err) => {
        console.error(err);
      });
    })
    .catch((err) => {
      console.error(err);
    });
  }
}).$mount('#app');
