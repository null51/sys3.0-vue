module.exports = {
  deploy: {
    // 'requestBaseUrl': 'http://192.168.128.107:8001/sys-service'
    'requestBaseUrl': 'http://localhost:9010/service'
  },
  clientCode: 'SYS3.1',
  previlegeCodes: {
    clients: {
      add: 'SYS_CLIENT_ADD',
      edit: '',
      disable: '',
      enable: ''
    },
    users: {
      add: 'SYS_USERS_ADD',
      edit: 'SYS_USERS_EDIT',
      bindRole: 'SYS_USERS_BIND_ROLE',
      reset: 'SYS_USERS_RESET_PASSWORD',
      cancel: 'SYS_USERS_CANCEL',
      freeze: 'SYS_USERS_FREEZE',
      unfreeze: 'SYS_USERS_UNFREEZE'
    },
    roles: {
      add: 'SYS_ROLES_ADD',
      edit: 'SYS_ROLES_EDIT',
      bindUser: 'SYS_ROLES_BIND_USER',
      delete: 'SYS_ROLES_DELETE'
    },
    resources: {},
  }
};
