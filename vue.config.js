/* eslint-disable */
const config = require('./src/config');
const path = require('path');
const getAssetPath = require('@vue/cli-service/lib/util/getAssetPath')
module.exports = {
  devServer: {
    open: true,
    port: 9000,
    proxy: {
      '/business': {
        target: `${config.deploy.requestBaseUrl}/business`,
        changeOrigin: true,
        pathRewrite: {
          '^/business': ''
        }
      },
      '/login': {
        target: `${config.deploy.requestBaseUrl}/login`,
        changeOrigin: true,
        pathRewrite: {
          '^/login': ''
        }
      },
      '/auth': {
        target: `${config.deploy.requestBaseUrl}/auth`,
        changeOrigin: true,
        pathRewrite: {
          '^/auth': ''
        }
      }
    }
  },
  configureWebpack: config => {
    config.resolve.alias = {
      '@': path.resolve(__dirname, './src'),
      '@assets': path.resolve(__dirname, './src/assets'),
      '@common': path.resolve(__dirname, './src/common'),
      '@components': path.resolve(__dirname, './src/components'),
      '@store': path.resolve(__dirname, './src/store'),
      '@router': path.resolve(__dirname, './src/router')
    };
    config.output.publicPath = './';
  },
  // chainWebpack: config => {
  //   var fontsRule = config.module.rule('fonts');
  //   fontsRule.use('url-loader')
  //   .tap(options => {
  //     console.dir(options)
  //     options.name = `../css/fonts/[name].[hash:8].[ext]`;
  //   })
  // }
};
